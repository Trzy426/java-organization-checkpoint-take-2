
package formatters;

import com.galvanize.Booking;

public class CSVFormatter implements Formatter {
    @Override
    public String format(Booking booking){
        return "type,room number,start time,end time\n" +
                String.format("%s,%s,%s,%s",
                        String.valueOf(booking.getRoomType()).replace("_", " "),
                        booking.getRoomNumber(),
                        booking.getStartTime(),
                        booking.getEndTime());
    }
}


