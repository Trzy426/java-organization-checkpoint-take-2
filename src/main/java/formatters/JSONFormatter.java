package formatters;

import com.galvanize.Booking;

public class JSONFormatter implements Formatter {
    @Override
    public String format(Booking booking) {

        return "{\n" +
                String.format("  \"type\": \"%s\",\n", String.valueOf(booking.getRoomType()).replace("_", " ")) +
                String.format("  \"roomNumber\": %s,\n", booking.getRoomNumber()) +
                String.format("  \"startTime\": \"%s\",\n", booking.getStartTime()) +
                String.format("  \"endTime\": \"%s\"\n", booking.getEndTime()) +
                "}";
    }
}

