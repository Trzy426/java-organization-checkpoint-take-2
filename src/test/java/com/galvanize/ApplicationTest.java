package com.galvanize;

import formatters.Formatter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static com.galvanize.Application.getFormatter;
import static org.junit.jupiter.api.Assertions.assertEquals;



    public class ApplicationTest {

        PrintStream original;
        ByteArrayOutputStream outContent;

        // This block captures everything written to System.out
        @BeforeEach
        public void setOut() {
            original = System.out;
            outContent = new ByteArrayOutputStream();
            System.setOut(new PrintStream(outContent));
        }

        // This block resets System.out to whatever it was before
        @AfterEach
        public void restoreOut() {
            System.setOut(original);
        }

        @Test
        public void FormatterCreationTest() {
            String expected = "<dl>\n" +
                    "  <dt>Type</dt><dd>Suite</dd>\n" +
                    "  <dt>Room Number</dt><dd>14</dd>\n" +
                    "  <dt>Start Time</dt><dd>10:30AM</dd>\n" +
                    "  <dt>End Time</dt><dd>12:30AM</dd>\n" +
                    "</dl>";
            Booking trip = Booking.parse("s14-10:30AM-12:30AM");
            Formatter actual = getFormatter("html");
            assertEquals(expected,actual.format(trip));

        }
        @Test
        public void TestMain(){
           Application app = new Application();

                String expected ="type,room number,start time,end time\n" +
                        "Suite,122,12:30pm,4:00pm";
                String[] params = {"s122-12:30pm-4:00pm","csv"};
                app.main(params);
                String actual = outContent.toString();
                assertEquals(expected,actual);


        }
    }
